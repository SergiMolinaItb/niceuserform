package cat.itb.niceuserform.screens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.Navigation;


import cat.itb.niceuserform.R;

public class HomeScreen extends Fragment {

    Button buttonLogin;
    Button buttonRegister;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_screen, container, false);

        buttonLogin = v.findViewById(R.id.loginButton);
        buttonRegister = v.findViewById(R.id.registerButton);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.loginButton) {
                    Navigation.findNavController(view).navigate(R.id.action_homeScreen_to_loginScreen);
                } else {
                    Navigation.findNavController(view).navigate(R.id.action_homeScreen_to_registerScreen);
                }
            }
        };

        buttonLogin.setOnClickListener(listener);
        buttonRegister.setOnClickListener(listener);
        return v;
    }
}
