package cat.itb.niceuserform.screens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.textfield.TextInputLayout;

import cat.itb.niceuserform.R;

public class LoginScreen extends Fragment {

    Button buttonLogin;
    Button buttonRegister;
    TextInputLayout textUsername;
    TextInputLayout textPassword;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_screen, container, false);

        buttonLogin = v.findViewById(R.id.loginButton);
        buttonRegister = v.findViewById(R.id.registerButton);
        textUsername = v.findViewById(R.id.username_text_input);
        textPassword = v.findViewById(R.id.password_text_input);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.loginButton) {
                    if (validateUsername() & validatePassword()) {
                        Navigation.findNavController(view).navigate(R.id.action_loginScreen_to_welcomeScreen);
                    }
                } else {
                    Navigation.findNavController(view).navigate(R.id.action_loginScreen_to_registerScreen);
                }
            }
        };

        buttonLogin.setOnClickListener(listener);
        buttonRegister.setOnClickListener(listener);



        return v;
    }

    private boolean validateUsername() {
        String usr = textUsername.getEditText().getText().toString().trim();
        if (usr.isEmpty()) {
            textUsername.setError("Required Field");
            return false;
        } else {
            textUsername.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String pass = textPassword.getEditText().getText().toString().trim();
        if (pass.isEmpty()) {
            textPassword.setError("Required Field");
            return false;
        } else {
            textPassword.setError(null);
            return true;
        }
    }
}
