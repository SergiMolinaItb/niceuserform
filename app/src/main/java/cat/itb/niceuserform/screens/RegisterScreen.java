package cat.itb.niceuserform.screens;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Calendar;
import java.util.TimeZone;

import cat.itb.niceuserform.R;

public class RegisterScreen extends Fragment {

    TextInputLayout username;
    TextInputLayout password;
    TextInputLayout rpassword;
    TextInputLayout email;
    TextInputLayout name;
    TextInputLayout surnames;
    TextInputLayout birth;
    TextInputEditText birthEdit;
    Button buttonRegister;
    public String pass;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.register_screen, container, false);

        buttonRegister = v.findViewById(R.id.registerButton);
        username = v.findViewById(R.id.username_text_input);
        password = v.findViewById(R.id.password_text_input);
        rpassword = v.findViewById(R.id.password_text_input2);
        email = v.findViewById(R.id.email_text_input);
        name = v.findViewById(R.id.name_text_input);
        surnames = v.findViewById(R.id.surnames_text_input);
        birth = v.findViewById(R.id.birth_text_input);
        birthEdit = v.findViewById(R.id.birth_text_edit);

        View.OnClickListener listener = new View.OnClickListener() {
            @SuppressLint("NonConstantResourceId")
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.birth_text_edit:
                        MaterialDatePicker.Builder<Pair<Long, Long>> builder = MaterialDatePicker.Builder.dateRangePicker();
                        builder.setTitleText("SELECCIONA UNA DATA");
                        final MaterialDatePicker materialDatePicker = builder.build();
                        materialDatePicker.show(getParentFragmentManager(), "DATA PICKER");

                        birthEdit.setText(materialDatePicker.getSelection().toString());
                        break;
                    case R.id.registerButton:
                        if (validation()) {
                            Navigation.findNavController(v).navigate(R.id.action_registerScreen_to_welcomeScreen);
                        }
                        break;
                }
            }
        };

        birth.setOnClickListener(listener);
        buttonRegister.setOnClickListener(listener);

        return v;
    }

    public boolean validation() {
        if (validateName() & validateDate() & validateEmail() & validateEmail() & validatePassword() & validateRpassword() & validateSname() & validateUsername()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean validateUsername() {
        String usr = username.getEditText().getText().toString().trim();
        if (usr.isEmpty()) {
            username.setError("Required Field");
            return false;
        } else {
            username.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        pass = password.getEditText().getText().toString().trim();
        if (pass.isEmpty()) {
            password.setError("Required Field");
            return false;
        } else if (pass.length() > 8) {
            password.setError("Password Must Have At Least 8 Characters");
            return false;
        } else {
            password.setError(null);
            return true;
        }
    }

    private boolean validateRpassword() {
        String rpass = rpassword.getEditText().getText().toString().trim();
        if (rpass.isEmpty()) {
            rpassword.setError("Required Field");
            return false;
        } else if (rpass.length() > 8) {
            rpassword.setError("Password Must Have At Least 8 Characters");
            return false;
        } else if (!rpass.equals(pass)) {
            rpassword.setError("Both Passwords Must Match");
            return false;
        } else {
            rpassword.setError(null);
            return true;
        }
    }

    private boolean validateEmail() {
        String mail =  email.getEditText().getText().toString().trim();
        if (mail.isEmpty()) {
            email.setError("Required Field");
            return false;
        } else {
            email.setError(null);
            return true;
        }
    }

    private boolean validateName() {
        String nm = name.getEditText().getText().toString().trim();
        if (nm.isEmpty()) {
            name.setError("Required Field");
            return false;
        } else {
            name.setError(null);
            return true;
        }
    }

    private boolean validateSname() {
        String snm = surnames.getEditText().getText().toString().trim();
        if (snm.isEmpty()) {
            surnames.setError("Required Field");
            return false;
        } else {
            surnames.setError(null);
            return true;
        }
    }

    private boolean validateDate() {
        String dt = birth.getEditText().getText().toString().trim();
        if (dt.isEmpty()) {
            birth.setError("Required Field");
            return false;
        } else {
            birth.setError(null);
            return true;
        }
    }
}
